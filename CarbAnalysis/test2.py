# -*- coding: utf-8 -*-

import cv2
import numpy as np
import imutils

from Tkinter import *
from PIL import Image
from PIL import ImageTk
import tkFileDialog
import os
import datetime


#----------------------------------------------------------------------

class MainWindow():

    found_circles = None
    img_color = 0   # 0 = greyscale, 1 = color
    img = None
    blurred_img = None
    img_path = None
    final_image = None
    min_radius = 10
    max_radius = 50
    min_distance_between_circles = 50
    highlight_color = [255,0,0] # Green
    white_color_pixel = 255
    radios = [] # We will save the radios of the detected circles
    original_image_x = 0
    original_image_y = 0
    resize_x = 800
    resize_y = 600
    image_zoom = 10
    pixel_to_mm_10x = 11    # 11 pixels = 1 mm
    pixel_to_mm_25x = 27    # 27 pixels = 1 mm

    start_corner_x = 0
    start_corner_y = 0
    end_corner_x = 0
    end_corner_y = 0
    circle = None

    def blur(self, img):
        blur_kernel_size = 5    # 5 x 5 filter
        return cv2.medianBlur(img, blur_kernel_size)

    def basic_threshold(self, img):
        #gray = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        return thresh

    def hough_transform(self):
        if type(self.blurred_img) is np.ndarray:
            accumulator_to_image_ratio = 1.8
            canny_threshold = 30
            accumulator_threshold = 28
            found_circles = cv2.HoughCircles(image = self.blurred_img,
            			                     method = cv2.HOUGH_GRADIENT,
                			                 dp = accumulator_to_image_ratio,
                                			 minDist = self.min_distance_between_circles,
                                             param1 = canny_threshold,
                                			 param2 = accumulator_threshold,
                                			 minRadius = self.min_radius,
                                			 maxRadius = self.max_radius)
            found_circles = found_circles[0]
            # Convert circles to integer for processing
            if type(found_circles) is np.ndarray:
                found_circles = np.uint16(np.around(found_circles))
                return found_circles
            else:
                return False

    def draw_circles(self, img, found_circles, highlight_index = -1):
        self.radios = []
        for circle_index, circle in enumerate(found_circles):
            # draw the outer circle
            if circle_index == highlight_index:
                cv2.circle(img, (circle[0],circle[1]),circle[2],(0,0,255),3)
            else:
                cv2.circle(img,(circle[0],circle[1]),circle[2],(255,0,0),3)
            radius_in_pixels = circle[2]
            radius_in_mm = float(radius_in_pixels / self.pixel_to_mm_10x)
            if image_zoom == 25:
                radius_in_mm = float(radius_in_pixels / self.pixel_to_mm_25x)
            radios.append(radius_in_mm)
        self.radius_label_text.set("Radio promedio: " + "{0:.3f}".format((float(sum(radios)/len(radios)))) + ' mm.')
        self.circle_label_text.set("Circulos encontrados: " + str(len(found_circles)))
        return img

    def save_found_circles_data(self, found_circles, window):
        current_month_and_year = datetime.date.today().strftime("%m-%Y")
        final_img_path = os.getcwd() + '/Results/Native/' + self.img_path.split('/')[-1]
        file_path = os.getcwd() + '/Results/Data/' + 'NATIVE_' + current_month_and_year + '.csv'
        file_writing_mode = 'a'    # append mode (writes on existent file)
        found_circle_data = ''
        if not os.path.isfile(file_path):
            found_circle_data = 'image type;celulose concentration;zoom;original image path;found circles;average radius;date;final image path\n'
            file_writing_mode = 'w'   # write mode (creates a new file)
        todays_file = open(file_path, file_writing_mode)
        final_img_path = save_final_image()
        found_circle_data += ("NATIVE;" + self.img_path.split('%')[0].split(' ')[1] + ";"
                                + self.image_zoom + ";"
                                + self.img_path + ";" + str(len(found_circles))
                                + ';' + str(sum(self.radios)/len(self.radios)) + ';'
                                + datetime.datetime.now().strftime("%m-%d-%Y %H:%M:%S")
                                + ';' + final_img_path + '\n')
        todays_file.write(found_circle_data)
        todays_file.close()

        window.destroy()

    def select_image(self):
        # open a file chooser dialog and allow the user to select an input
        # image
        self.img_path = tkFileDialog.askopenfilename()
        self.image_zoom = int(self.img_path.split('x')[1].split('_')[0].split('P')[0])
        # ensure a file path was selected
        if len(self.img_path) > 0:
            self.img = cv2.imread(self.img_path, self.img_color)
            self.min_distance_between_circles_slider.pack()
            self.min_radius_slider.pack()
            self.max_radius_slider.pack()
            self.circle_label.pack()
            self.radius_label.pack()
            self.next_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
            self.photo_btn.config(text = "Cambiar imagen")
            self.process_image()

    def reprocess_image(self, slider):
        self.blurred_img = self.blur(self.img)

        # Threshold the image for border detection
        image = cv2.cvtColor(self.blurred_img, cv2.COLOR_GRAY2BGR)
        thresholded_img = self.basic_threshold(image)
        # Remove noise (small floating particles) by blurring
        blurred_img = self.blur(thresholded_img)
        if type(self.img) is np.ndarray:
            min_radius = self.min_radius_slider.get()
            max_radius = self.max_radius_slider.get()
            self.min_distance_between_circles = self.min_distance_between_circles_slider.get()
            # Hough Transform to find circles
            self.found_circles = self.hough_transform()
            self.raw_img = cv2.imread(self.img_path, 1)
            if type(self.found_circles) is not bool:
                # Draw the found circles on the original image and save it
                self.img_with_circles = self.draw_circles(self.raw_img, self.found_circles)
                self.final_image = self.img_with_circles
                self.show_image(self.img_with_circles)
            else:
                self.circle_label_text.set("Circulos encontrados: 0")
                self.final_image = self.raw_img
                self.show_image(self.raw_img)


    def process_image(self):
        # Apply blur to remove noise
        self.blurred_img = self.blur(self.img)
        # Threshold the image for border detection
        self.image = cv2.cvtColor(self.blurred_img, cv2.COLOR_GRAY2BGR)
        self.thresholded_img = self.basic_threshold(self.image)
        # Remove noise (small floating particles) by blurring
        self.blurred_img = blur(self.thresholded_img)
        # Hough Transform to find circles
        self.found_circles = self.hough_transform()
        # Draw the found circles on the original image and save it
        self.raw_img = cv2.imread(self.img_path, 1)
        self.img_with_circles = draw_circles(self.raw_img, self.found_circles)
        self.final_image = self.img_with_circles
        self.show_image(self.img_with_circles)


    def save_final_image(self):
        final_img_path = os.getcwd() + '/Results/Native/' + self.img_path.split('/')[-1]
        cv2.imwrite(final_img_path, self.final_image)
        return final_img_path


    def show_image(self, img):
        # SHOW IN GUI
        # Convert to COLOR_BGR
        image = img
        #image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        # OpenCV represents images in BGR order; however PIL represents
        # images in RGB order, so we need to swap the channels
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # convert the images to PIL format...
        image = Image.fromarray(image)
        original_image_x, original_image_y = image.size
        image = image.resize((resize_x, resize_y),Image.ANTIALIAS)
        # ...and then to ImageTk format
        image = ImageTk.PhotoImage(image)
        # if the panels are None, initialize them
        self.canvas.itemconfig(self.image_on_canvas, image = image)

    def convert_point(self, coordenate_x, coordenate_y):
        x_ratio = float(self.original_image_x)/self.resize_x
        y_ratio = float(self.original_image_y)/self.resize_y
        return coordenate_x * x_ratio, coordenate_y * y_ratio

    def center_window(self, toplevel):
        toplevel.update_idletasks()
        w = toplevel.winfo_screenwidth()
        h = toplevel.winfo_screenheight()
        size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2
        toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))


    def select_circle(self, click_event):
        x, y  = convert_point(click_event.x, click_event.y)
        found_index = -1
        raw_img = cv2.imread(self.img_path, 1)
        for circle_index, circle in enumerate(self.found_circles):
            center_x = circle[0]
            center_y = circle[1]
            radius = circle[2]
            if ((x - center_x)**2 + (y - center_y)**2) < radius**2:
                self.is_circle_selected = True
                self.found_index = circle_index
                self.img_with_circles = self.draw_circles(raw_img, self.found_circles, self.found_index)
                self.show_image(self.img_with_circles)
                toplevel = Toplevel()
                self.delete_circle_btn = Button(toplevel, text="Eliminar Circulo", command=lambda : self.delete_circle(circle_index, toplevel))
                self.delete_circle_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
                center_window(toplevel)
                break


    def delete_circle(self, index, toplevel):
        raw_img = cv2.imread(self.img_path, 1)
        new_circles = np.array(np.delete(self.found_circles, index, axis=0))
        self.found_circles = new_circles
        self.img_with_circles = self.draw_circles(raw_img, self.found_circles)
        self.final_image = self.img_with_circles
        self.show_image(self.img_with_circles)
        self.is_circle_selected = False
        toplevel.destroy()

    def callback(self, event):
        self.draw(event.x, event.y)

    def draw(self, x, y):
        self.canvas.coords(self.circle, self.start_corner_x, self.start_corner_y, x, y)

    def on_start(self, event):
        self.canvas.configure(cursor="hand1")
        # you could use this method to create a floating window
        # that represents what is being dragged.
        self.start_corner_x = event.x
        self.start_corner_y = event.y
        print str(event.x) + ',' + str(event.y)

    def on_drag(self, event):
        self.end_corner_x = event.x
        self.end_corner_y = event.y
        dif_x = (self.start_corner_x - self.end_corner_x)
        dif_y = (self.start_corner_y - self.end_corner_y)
        if abs(dif_x) < abs(dif_y): # hay que dejar dif_y = dif_x
            if dif_y < 0:
                end_corner_y = end_corner_y + (abs(dif_x) - abs(dif_y))
            else:
                end_corner_y = end_corner_y - (abs(dif_x) - abs(dif_y))
        else:   # hay que dejar dif_x = dif_y
            if dif_x < 0:
                end_corner_x = end_corner_x + (abs(dif_y) - abs(dif_x))
            else:
                end_corner_x = end_corner_x - (abs(dif_y) - abs(dif_x))
        draw(end_corner_x, end_corner_y)
        print str(end_corner_x) + ',' + str(end_corner_y)

    def on_drop(self, event):
        global start_corner_x
        global start_corner_y
        global end_corner_x
        global end_corner_y
        # find the widget under the cursor
        center_x = start_corner_x + (end_corner_x - start_corner_x)/2
        center_y = start_corner_y + (end_corner_y - start_corner_y)/2
        radius = abs(start_corner_x - end_corner_x)/2
        print 'center (x,y): ' + str(center_x) + ',' + str(center_y) + ' / radius: ' + str(radius)

    #----------------

    def __init__(self, main):
        main.title("Reconocimiento Muestra Pre-Procesamiento")
        self.canvas = Canvas(main, width=self.resize_x, height=self.resize_y, bg='white')
        self.canvas.pack(expand=YES, fill=BOTH)
        temp_img = ImageTk.PhotoImage(file='hist.png')
        self.image_on_canvas = self.canvas.create_image(0, 0, image=temp_img, anchor=NW)
        self.canvas.bind("<ButtonPress-1>", self.on_start)
        self.canvas.bind("<B1-Motion>", self.on_drag)
        self.canvas.bind("<ButtonRelease-1>", self.on_drop)
        self.canvas.bind("<Button 1>", self.select_circle)
        self.circle = self.canvas.create_oval(0, 0, 0, 0)
        self.canvas.pack()

        self.min_distance_var = DoubleVar()
        self.min_distance_between_circles_slider = Scale( main, length=200, from_=1, to=150, label="Distancia entre Centros (px):", variable = self.min_distance_var, orient="horizontal", command=self.reprocess_image)
        self.min_distance_between_circles_slider.set(50)
        self.min_radius_var = DoubleVar()
        self.min_radius_slider = Scale( main,  length=200, from_=1, to=250, label="Radio Minimo (px):", variable = self.min_radius_var, orient="horizontal", command=self.reprocess_image )
        self.min_radius_slider.set(10)
        self.max_radius_var = DoubleVar()
        self.max_radius_slider = Scale( main,  length=200, from_=1, to=250, label="Radio Máximo (px):", variable = self.max_radius_var, orient="horizontal", command=self.reprocess_image )
        self.max_radius_slider.set(50)
        self.circle_label_text = StringVar()
        self.circle_label = Label(main, textvariable=self.circle_label_text)
        self.radius_label_text = StringVar()
        self.radius_label = Label(main, textvariable=self.radius_label_text)

        self.photo_btn = Button(main, text="Seleccionar imagen", command= lambda : self.select_image(), width = 80)
        self.photo_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
        self.next_btn = Button(main, text="Siguiente", command= lambda : self.save_found_circles_data(found_circles, root))

    #----------------


#----------------------------------------------------------------------

root = Tk()
MainWindow(root)
root.mainloop()
