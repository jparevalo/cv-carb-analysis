import trackpy as tp
import numpy as np
import pandas as pd
import cv2 as cv
import pims
import matplotlib.pyplot as plt

#%% importing the data
frames=pims.ImageSequence('a.jpg')

#%% tracking circles and center positions
featuresize=21
f1=tp.locate(cv.cvtColor(frames[0],cv.COLOR_BGR2GRAY),featuresize)

plt.figure()
tp.annotate(f1,frames[0])
