import cv2
import numpy as np


original = cv2.imread("a.jpg", 0)
ret, image = cv2.threshold(original,50,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
cv2.imwrite('thresh.png', image)


el = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
image = cv2.dilate(image, el, iterations=9)

cv2.imwrite("dilated.png", image)

a, contours, hierarchy = cv2.findContours(
    image,
    cv2.RETR_LIST,
    cv2.CHAIN_APPROX_SIMPLE
)

drawing = cv2.imread("a.jpg")

centers = []
radii = []
for contour in contours:
    area = cv2.contourArea(contour)

    # there is one contour that contains all others, filter it out
    if area > 4000:
        continue

    br = cv2.boundingRect(contour)
    radii.append(br[2])

    m = cv2.moments(contour)
    center = (int(m['m10'] / m['m00']), int(m['m01'] / m['m00']))
    centers.append(center)

print("There are {} circles".format(len(centers)))

radius = int(np.average(radii)) + 5

for i,center in enumerate(centers):
    cv2.circle(drawing, center, 3, (0, 0, 0), -1)
    cv2.circle(drawing, center, radii[i], (255, 0, 0), 3)

cv2.imshow("output", drawing)
cv2.waitKey(0)
