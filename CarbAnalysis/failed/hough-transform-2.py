# -*- coding: utf-8 -*-

import cv2
import numpy as np
import imutils

from Tkinter import *
from PIL import Image
from PIL import ImageTk
import tkFileDialog

found_circles = None
img_color = 0   # 0 = greyscale, 1 = color
img = None
blurred_img = None
img_path = None
min_radius = 10
max_radius = 50
min_distance_between_circles = 50
highlight_color = [255,0,0] # Green
white_color_pixel = 255
radios = [] # We will save the radios of the detected circles
original_image_x = 0
original_image_y = 0
resize_x = 800
resize_y = 600



def blur(img):
    blur_kernel_size = 5    # 5 x 5 filter
    return cv2.medianBlur(img, blur_kernel_size)

def basic_threshold(img):
    #gray = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    return thresh

def find_contours(img):
    img, contours, hierarchy = cv2.findContours(image = img,
                                                mode = cv2.RETR_TREE,
                                                method = cv2.CHAIN_APPROX_SIMPLE)
    return contours

def remove_background(img, white_background_image):
    for row_index, row in enumerate(img):
        for pixel_index, pixel in enumerate(row):
            thresholded_pixel = white_background_image[row_index][pixel_index]
            if thresholded_pixel != 255:
                white_background_image[row_index][pixel_index] = pixel
    return white_background_image

def hough_transform():
    global blurred_img
    global min_radius
    global max_radius
    global min_distance_between_circles
    global found_circles
    if type(blurred_img) is np.ndarray:
        accumulator_to_image_ratio = 1
        canny_threshold = 30
        accumulator_threshold = 28
        found_circles = cv2.HoughCircles(image = blurred_img,
        			                     method = cv2.HOUGH_GRADIENT,
            			                 dp = accumulator_to_image_ratio,
                            			 minDist = min_distance_between_circles,
                                         param1 = canny_threshold,
                            			 param2 = accumulator_threshold,
                            			 minRadius = min_radius,
                            			 maxRadius = max_radius)
        found_circles = found_circles[0]
        # Convert circles to integer for processing
        if type(found_circles) is np.ndarray:
            found_circles = np.uint16(np.around(found_circles))
            return found_circles
        else:
            return False

def draw_circles(img, found_circles, highlight_index = -1):
    for circle_index, circle in enumerate(found_circles):
        # draw the outer circle
        if circle_index == highlight_index:
            cv2.circle(img, (circle[0],circle[1]),circle[2],(0,0,255),3)
        else:
            cv2.circle(img,(circle[0],circle[1]),circle[2],(255,0,0),3)
        radios.append(circle[2])
    return img

def fill_circles(img, found_circles):
    for i in found_circles:
        # draw the outer circle
        cv2.circle(img, (i[0],i[1]),i[2]+5,(255,255,255),-2)
    return img

def print_results(found_circles, pix_to_nano_ratio):
    print '---------------------HOUGH--TRANSFORM-------------------------'
    print 'CIRCULOS INICIALES:',len(found_circles)
    print 'RADIO PROMEDIO INICIAL:', str(round(sum(radios)/len(radios),2))+' px ('+str(round(float(sum(radios)/len(radios))*pix_to_nano_ratio,2))+' μm)'

def save_found_circles_data(found_circles):
    file = open('circulos.txt', 'w')
    text = (str(len(found_circles)) + '\n' + str(sum(radios)/len(radios)))
    file.write(text)
    file.close()

def select_image():
    global img
    global img_path
    # open a file chooser dialog and allow the user to select an input
    # image
    img_path = tkFileDialog.askopenfilename()
    # ensure a file path was selected
    if len(img_path) > 0:
        img = cv2.imread(img_path, img_color)
        min_distance_between_circles_slider.pack()
        min_radius_slider.pack()
        max_radius_slider.pack()
        circle_label.pack()
        next_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
        photo_btn.config(text = "Cambiar imagen")
        process_image()

def watershed():
    global blurred_img
    # noise removal
    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)
    # sure background area
    sure_bg = cv2.dilate(opening,kernel,iterations=3)
    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg,sure_fg)
    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg)
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1
    # Now, mark the region of unknown with zero
    markers[unknown==255] = 0
    markers = cv2.watershed(img,markers)
    img = cv2.imread(img_path)
    img[markers == -1] = [255,0,0]
    return img


def reprocess_image(slider):
    global img
    global min_radius
    global max_radius
    global min_distance_between_circles
    blurred_img = blur(img)

    # Threshold the image for border detection
    image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    thresholded_img = basic_threshold(image)
    cv2.imwrite('thresh.png', thresholded_img)
    # Find contours
    #contours = find_contours(thresholded_img)
    # Remove noise (small floating particles) by blurring
    blurred_img = blur(thresholded_img)
    if type(img) is np.ndarray:
        min_radius = min_radius_slider.get()
        max_radius = max_radius_slider.get()
        min_distance_between_circles = min_distance_between_circles_slider.get()
        # Hough Transform to find circles
        found_circles = hough_transform()
        raw_img = cv2.imread(img_path, 1)
        if type(found_circles) is not bool:
            # Draw the found circles on the original image and save it
            circle_label_text.set("Circulos encontrados: " + str(len(found_circles)))
            img_with_circles = draw_circles(raw_img, found_circles)
            show_image(img_with_circles)
        else:
            circle_label_text.set("Circulos encontrados: 0")
            show_image(raw_img)


def process_image():
    global img
    global blurred_img
    # Apply blur to remove noise
    blurred_img = blur(img)

    # Threshold the image for border detection
    image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    thresholded_img = basic_threshold(image)
    cv2.imwrite('thresh.png', thresholded_img)
    # Find contours
    #contours = find_contours(thresholded_img)
    # Remove noise (small floating particles) by blurring
    blurred_img = blur(thresholded_img)
    # Hough Transform to find circles
    found_circles = hough_transform()
    circle_label_text.set("Circulos encontrados: " + str(len(found_circles)))
    # Draw the found circles on the original image and save it
    raw_img = cv2.imread(img_path, 1)
    img_with_circles = draw_circles(raw_img, found_circles)
    show_image(img_with_circles)
    #save_image_data(found_circles)


def save_image_data(found_circles):
    global blurred_img
    global img
    cv2.imwrite('original_first.png', img)
    # Fill the circles with white for further processing (so they become background) and save it
    blurred_img = fill_circles(blurred_img, found_circles)
    cv2.imwrite('first_iteration.png', blurred_img)
    # Get ratio between pixels and nanometers
    height, width = img.shape
    pix_to_nano_ratio = float(5)/height
    # Print results of hough transform circle detection
    print_results(found_circles, pix_to_nano_ratio)
    # Save circle data for further processing
    save_found_circles_data(found_circles)

def show_image(img):
    global panelA
    global resize_x
    global resize_y
    global original_image_x
    global original_image_y
    # SHOW IN GUI
    # Convert to COLOR_BGR
    image = img
    #image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # OpenCV represents images in BGR order; however PIL represents
    # images in RGB order, so we need to swap the channels
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # convert the images to PIL format...
    image = Image.fromarray(image)
    original_image_x, original_image_y = image.size
    image = image.resize((resize_x, resize_y),Image.ANTIALIAS)
    # ...and then to ImageTk format
    image = ImageTk.PhotoImage(image)

    # if the panels are None, initialize them
    if panelA is None:
        # the first panel will store our original image
        panelA = Label(image=image)
        panelA.image = image
        panelA.pack(side="left", padx=10, pady=10)
        panelA.bind("<Button 1>",select_circle)

    # otherwise, update the image panels
    else:
        # update the pannels
        panelA.configure(image=image)
        panelA.image = image

def convert_point(coordenate_x, coordenate_y):
    global original_image_x
    global original_image_y
    global resize_x
    global resize_y
    x_ratio = float(original_image_x)/resize_x
    y_ratio = float(original_image_y)/resize_y
    return coordenate_x * x_ratio, coordenate_y * y_ratio

def center_window(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    x = w/2 - size[0]/2
    y = h/2 - size[1]/2
    toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))


def select_circle(click_event):
    global found_circles
    global is_circle_selected
    x, y  = convert_point(click_event.x, click_event.y)
    found_index = -1
    raw_img = cv2.imread(img_path, 1)
    for circle_index, circle in enumerate(found_circles):
        center_x = circle[0]
        center_y = circle[1]
        radius = circle[2]
        if ((x - center_x)**2 + (y - center_y)**2) < radius**2:
            print radius
            is_circle_selected = True
            found_index = circle_index
            img_with_circles = draw_circles(raw_img, found_circles, found_index)
            show_image(img_with_circles)
            toplevel = Toplevel()
            delete_circle_btn = Button(toplevel, text="Eliminar Circulo", command=lambda : delete_circle(circle_index, toplevel))
            delete_circle_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
            center_window(toplevel)
            break


def delete_circle(index, toplevel):
    global found_circles
    raw_img = cv2.imread(img_path, 1)
    print len(found_circles)
    new_circles = np.array(np.delete(found_circles, index, axis=0))
    found_circles = new_circles
    img_with_circles = draw_circles(raw_img, found_circles)
    show_image(img_with_circles)
    is_circle_selected = False
    toplevel.destroy()

def next_step():
    return 1

if __name__ == '__main__':
    global panelA
    root = Tk()
    root.title("Reconocimiento")
    panelA = None

    min_distance_var = DoubleVar()
    min_distance_between_circles_slider = Scale( root, length=200, from_=1, to=100, label="Distancia entre Centros (px):", variable = min_distance_var, orient="horizontal", command=reprocess_image )
    min_distance_between_circles_slider.set(50)
    min_radius_var = DoubleVar()
    min_radius_slider = Scale( root,  length=200, from_=1, to=150, label="Radio Minimo (px):", variable = min_radius_var, orient="horizontal", command=reprocess_image )
    min_radius_slider.set(10)
    max_radius_var = DoubleVar()
    max_radius_slider = Scale( root,  length=200, from_=1, to=350, label="Radio Máximo (px):", variable = max_radius_var, orient="horizontal", command=reprocess_image )
    max_radius_slider.set(50)
    circle_label_text = StringVar()
    circle_label = Label(root, textvariable=circle_label_text)

    photo_btn = Button(root, text="Seleccionar imagen", command=select_image)
    photo_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
    next_btn = Button(root, text="Siguiente", command=next_step)

    root.mainloop()
