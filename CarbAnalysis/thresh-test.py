# -*- coding: utf-8 -*-

import cv2
import numpy as np
import imutils

from Tkinter import *
from PIL import Image
from PIL import ImageTk
import tkFileDialog

min_t = 0
max_t = 255
found_circles = None
img_color = 0   # 0 = greyscale, 1 = color
img = None
blurred_img = None
img_path = None
min_distance_between_circles = 50
highlight_color = [255,0,0] # Green
white_color_pixel = 255
radios = [] # We will save the radios of the detected circles
original_image_x = 0
original_image_y = 0
resize_x = 800
resize_y = 600

def basic_threshold(img):
    #gray = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray,min_t,max_t,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    return thresh

def select_image():
    global img
    global img_path
    # open a file chooser dialog and allow the user to select an input
    # image
    img_path = tkFileDialog.askopenfilename()
    # ensure a file path was selected
    if len(img_path) > 0:
        img = cv2.imread(img_path, 1)
        min_t_slider.pack()
        max_t_slider.pack()
        photo_btn.config(text = "Cambiar imagen")
        process_image()

def show_image(img):
    global panelA
    global resize_x
    global resize_y
    global original_image_x
    global original_image_y
    # SHOW IN GUI
    # Convert to COLOR_BGR
    image = img
    #image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # OpenCV represents images in BGR order; however PIL represents
    # images in RGB order, so we need to swap the channels
    #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # convert the images to PIL format...
    image = Image.fromarray(image)
    original_image_x, original_image_y = image.size
    image = image.resize((resize_x, resize_y),Image.ANTIALIAS)
    # ...and then to ImageTk format
    image = ImageTk.PhotoImage(image)

    # if the panels are None, initialize them
    if panelA is None:
        # the first panel will store our original image
        panelA = Label(image=image)
        panelA.image = image
        panelA.pack(side="left", padx=10, pady=10)

    # otherwise, update the image panels
    else:
        # update the pannels
        panelA.configure(image=image)
        panelA.image = image


def reprocess_image(slider):
    global img
    global min_t
    global max_t
    if type(img) is np.ndarray:
        min_t = min_t_slider.get()
        max_t = max_t_slider.get()
        th = basic_threshold(img)
        show_image(th)


def process_image():
    global img
    if type(img) is np.ndarray:
        th = basic_threshold(img)
        show_image(th)

if __name__ == '__main__':
    global panelA
    root = Tk()
    root.title("Threshold")
    panelA = None

    min_t_var = DoubleVar()
    min_t_slider = Scale( root,  length=200, from_=0, to=255, label="Minimo (px):", variable = min_t_var, orient="horizontal", command=reprocess_image )
    min_t_slider.set(0)
    max_t_var = DoubleVar()
    max_t_slider = Scale( root,  length=200, from_=0, to=255, label="Máximo (px):", variable = max_t_var, orient="horizontal", command=reprocess_image )
    max_t_slider.set(255)

    photo_btn = Button(root, text="Seleccionar imagen", command=select_image)
    photo_btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")

    root.mainloop()
