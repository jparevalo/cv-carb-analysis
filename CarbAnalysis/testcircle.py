from Tkinter import Tk, Canvas

start_corner_x = 0
start_corner_y = 0
end_corner_x = 0
end_corner_y = 0

def callback(event):
    draw(event.x, event.y)

def draw(x, y):
    global start_corner_x
    global start_corner_y
    widget.coords(circle, start_corner_x, start_corner_y, x, y)

def on_start(event):
    global start_corner_x
    global start_corner_y
    widget.configure(cursor="hand1")
    # you could use this method to create a floating window
    # that represents what is being dragged.
    start_corner_x = event.x
    start_corner_y = event.y
    print str(event.x) + ',' + str(event.y)

def on_drag(event):
    global end_corner_x
    global end_corner_y
    global start_corner_x
    global start_corner_y
    end_corner_x = event.x
    end_corner_y = event.y
    dif_x = (start_corner_x - end_corner_x)
    dif_y = (start_corner_y - end_corner_y)
    if abs(dif_x) < abs(dif_y): # hay que dejar dif_y = dif_x
        if dif_y < 0:
            end_corner_y = end_corner_y + (abs(dif_x) - abs(dif_y))
        else:
            end_corner_y = end_corner_y - (abs(dif_x) - abs(dif_y))
    else:   # hay que dejar dif_x = dif_y
        if dif_x < 0:
            end_corner_x = end_corner_x + (abs(dif_y) - abs(dif_x))
        else:
            end_corner_x = end_corner_x - (abs(dif_y) - abs(dif_x))
    draw(end_corner_x, end_corner_y)
    print str(end_corner_x) + ',' + str(end_corner_y)

def on_drop(event):
    global start_corner_x
    global start_corner_y
    global end_corner_x
    global end_corner_y
    # find the widget under the cursor
    center_x = start_corner_x + (end_corner_x - start_corner_x)/2
    center_y = start_corner_y + (end_corner_y - start_corner_y)/2
    radius = abs(start_corner_x - end_corner_x)/2
    print 'center (x,y): ' + str(center_x) + ',' + str(center_y) + ' / radius: ' + str(radius)

root = Tk()
widget = Canvas(root)
widget.bind("<ButtonPress-1>", on_start)
widget.bind("<B1-Motion>", on_drag)
widget.bind("<ButtonRelease-1>", on_drop)
widget.pack()

circle = widget.create_oval(0, 0, 0, 0)
root.mainloop()
