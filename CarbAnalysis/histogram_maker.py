import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.figure_factory as ff
import numpy as np

import json
import itertools

# plotly.tools.set_credentials_file(username='huarn93', api_key='PlfCXeDzRERi2TYgMAsJ')
#
# x = np.random.randn(500)
# data = [go.Histogram(x=x)]
#
# py.iplot(data, filename='basic histogram')

n_radius = []       #
p_radius = []       #
n_h_radius = []
n_d_radius = []
p_h_radius = []
p_d_radius = []
n_average = []      #
p_average = []      #
n_h_average = []
p_h_average = []
n_d_average = []
p_d_average = []
n_found = []        #
p_found = []        #
n_h_found = []
p_h_found = []
n_d_found = []
p_d_found = []
n_deleted = []
p_deleted = []

zero_index = 3
two_index = 10

processed_e = open('Results/Data/PROCESSED_ERROR_06-2018.csv', 'r').readlines()
native_e = open('Results/Data/NATIVE_ERROR_06-2018.csv', 'r').readlines()
processed = open('Results/Data/PROCESSED_06-2018.csv', 'r').readlines()
native = open('Results/Data/NATIVE_06-2018.csv', 'r').readlines()
comparison = open('Results/Data/COMPARISON_06-2018.csv', 'r').readlines()

for i in range(len(processed_e)):
    if i == 0:
        continue
    n_found.append(comparison[i].strip().split(';')[6])
    p_found.append(comparison[i].strip().split(';')[7])
    n_average.append(comparison[i].strip().split(';')[8])
    p_average.append(comparison[i].strip().split(';')[9])
    n_h_found.append(native_e[i].strip().split(';')[4])
    n_d_found.append(native_e[i].strip().split(';')[5])
    n_radius.append(json.loads(native_e[i].strip().split(';')[6]))
    n_h_radius.append(json.loads(native_e[i].strip().split(';')[7]))
    n_d_radius.append(json.loads(native_e[i].strip().split(';')[8]))
    n_h_average.append(native_e[i].strip().split(';')[10])
    n_d_average.append(native_e[i].strip().split(';')[11])
    n_deleted.append(native_e[i].strip().split(';')[12])
    p_h_found.append(processed_e[i].strip().split(';')[4])
    p_d_found.append(processed_e[i].strip().split(';')[5])
    p_radius.append(json.loads(processed_e[i].strip().split(';')[6]))
    p_h_radius.append(json.loads(processed_e[i].strip().split(';')[7]))
    p_d_radius.append(json.loads(processed_e[i].strip().split(';')[8]))
    p_h_average.append(processed_e[i].strip().split(';')[10])
    p_d_average.append(processed_e[i].strip().split(';')[11])
    p_deleted.append(processed_e[i].strip().split(';')[12])

# n_zero_radius = []
# p_zero_radius = []
# n_two_radius = []
# p_two_radius = []
# n_ten_radius = []
# p_ten_radius = []
#
# for index, r in enumerate(n_radius):
#     for k, v in r.iteritems():
#         if index <= zero_index:
#             n_zero_radius += [int(k)]*int(v)
#         elif index <= two_index:
#             n_two_radius += [int(k)]*int(v)
#         else:
#             n_ten_radius += [int(k)]*int(v)
#
# for index, r in enumerate(p_radius):
#     for k, v in r.iteritems():
#         if index <= zero_index:
#             p_zero_radius += [int(k)]*int(v)
#         elif index <= two_index:
#             p_two_radius += [int(k)]*int(v)
#         else:
#             p_ten_radius += [int(k)]*int(v)
#
# group_labels = ['Native', 'Processed']
#
# # Create distplot with custom bin_size
# fig = ff.create_distplot([n_ten_radius, p_ten_radius], group_labels, bin_size=3)
#
# # Plot!
# py.iplot(fig, filename='10% celulose - Radius Distribution Native vs Processed')

#
# n_h_total = []
# n_d_total = []
# p_h_total = []
# p_d_total = []
#
# for index, r in enumerate(n_h_radius):
#     for k, v in r.iteritems():
#         n_h_total += [int(k)]*int(v)
#
# for index, r in enumerate(n_d_radius):
#     for k, v in r.iteritems():
#         n_d_total += [int(k)]*int(v)
#
# for index, r in enumerate(p_h_radius):
#     for k, v in r.iteritems():
#         p_h_total += [int(k)]*int(v)
#
# for index, r in enumerate(p_d_radius):
#     for k, v in r.iteritems():
#         p_d_total += [int(k)]*int(v)
#
# data = [trace1, trace2]
# layout = go.Layout(barmode='overlay')
# fig = go.Figure(data=data, layout=layout)
# py.iplot(fig, filename='PROCESSED - Drawn vs Hough circle radius')

# Create traces
# trace0 = go.Scatter(
#     x = range(len(n_average)),
#     y = n_average,
#     mode = 'lines+markers',
#     name = 'Total Radii'
# )
# trace1 = go.Scatter(
#     x = range(len(n_h_average)),
#     y = n_h_average,
#     mode = 'lines+markers',
#     name = 'Hough Radii'
# )
# trace2 = go.Scatter(
#     x = range(len(n_d_average)),
#     y = n_d_average,
#     mode = 'lines+markers',
#     name = 'Drawn Radii'
# )

# trace0 = go.Scatter(
#     x = range(len(p_average)),
#     y = p_average,
#     mode = 'lines+markers',
#     name = 'Total Radii'
# )
# trace1 = go.Scatter(
#     x = range(len(p_h_average)),
#     y = p_h_average,
#     mode = 'lines+markers',
#     name = 'Hough Radii'
# )
# trace2 = go.Scatter(
#     x = range(len(p_d_average)),
#     y = p_d_average,
#     mode = 'lines+markers',
#     name = 'Drawn Radii'
# )
# data = [trace0, trace1, trace2]
# py.iplot(data, filename='PROCESSED - Hough vs Drawn Radii')

acc_z = 0
acc_tw = 0
acc_te = 0
tp = map(int, p_h_found)
fp = map(int, p_deleted)
fn = map(int, p_d_found)

# 10 = 0,4,5,6,7,8,11,12,13,14
# 25 = 1,2,3,9,10

acc_ten = float(sum([tp[0]]+tp[4:9]+tp[11:]))/sum([tp[0]]+tp[4:9]+tp[11:] + [fn[0]]+fn[4:9]+fn[11:])
acc_tf = float(sum(tp[1:4]+tp[9:11]))/sum(tp[1:4]+tp[9:11] + fn[1:4]+fn[9:11])
rec_ten = float(sum([tp[0]]+tp[4:9]+tp[11:]))/sum([tp[0]]+tp[4:9]+tp[11:] + [fp[0]]+fp[4:9]+fp[11:])
rec_tf = float(sum(tp[1:4]+tp[9:11]))/sum(tp[1:4]+tp[9:11] + fp[1:4]+fp[9:11])

# rec_z = 0
# rec_tw = 0
# rec_te = 0
# tp = map(int, p_h_found)
# fp = map(int, p_deleted)
# fn = map(int, p_d_found)
#
# rec_z = float(sum(tp[:zero_index+1]))/sum(tp[:zero_index+1] + fn[:zero_index+1])
# rec_tw = float(sum(tp[zero_index:two_index+1]))/sum(tp[zero_index:two_index+1] + fn[zero_index:two_index+1])
# rec_te = float(sum(tp[two_index:]))/sum(tp[two_index:] + fn[two_index:])
#
#
ten = go.Bar(
            x=['x10', 'x25'],
            y=[acc_ten, acc_tf],
            text=[acc_ten, acc_tf],
                         textposition = 'auto',
                         marker=dict(
                             color='rgb(158,202,225)',
                             line=dict(
                                 color='rgb(8,48,107)',
                                 width=1.5),
                         ),
                         opacity=0.6
    )

tf = go.Bar(
            x=['x10', 'x25'],
            y=[rec_ten, rec_tf],
            text=[rec_ten, rec_tf],
                         textposition = 'auto',
                         marker=dict(
                             color='rgb(58,200,225)',
                            line=dict(
                                color='rgb(8,48,107)',
                                width=1.5),
                         ),
                         opacity=0.6
    )

py.iplot([ten, tf], filename='PROCESSED - ACCURACY AND RECALL BY ZOOM')

# n_average_z = sum(map(float, n_average[:zero_index+1]))/len(n_average[:zero_index+1])
# n_average_tw = sum(map(float, n_average[zero_index+3:two_index+1]))/len(n_average[zero_index+3:two_index+1])
# n_average_te = sum(map(float, n_average[two_index:]))/len(n_average[two_index:])
# p_average_z = sum(map(float, p_average[:zero_index+1]))/len(n_average[:zero_index+1])
# p_average_tw = sum(map(float, p_average[zero_index+3:two_index+1]))/len(n_average[zero_index+3:two_index+1])
# p_average_te = sum(map(float, p_average[two_index:]))/len(n_average[two_index:])
#
# x = ['Celulose 0%', 'Celulose 2%', 'Celulose 10%']

# trace1 = go.Bar(
#     x=x,
#     y=[n_average_z, n_average_tw, n_average_te],
#     text=[n_average_z, n_average_tw, n_average_te],
#     textposition = 'auto',
#     marker=dict(
#         color='rgb(158,202,225)',
#         line=dict(
#             color='rgb(8,48,107)',
#             width=1.5),
#         ),
#     opacity=0.6
# )
#
# trace2 = go.Bar(
#     x=x,
#     y=[p_average_z, p_average_tw, p_average_te],
#     text=[p_average_z, p_average_tw, p_average_te],
#     textposition = 'auto',
#     marker=dict(
#         color='rgb(58,200,225)',
#         line=dict(
#             color='rgb(8,48,107)',
#             width=1.5),
#         ),
#     opacity=0.6
# )
# data = [trace1,trace2]
#
# py.iplot(data, filename='Radii comparative Native vs Processed 2')

# data = [go.Bar(
#             x=x,
#             y=[p_average_z-n_average_z, p_average_tw-n_average_tw, p_average_te-n_average_te],
#             text=[p_average_z-n_average_z, p_average_tw-n_average_tw, p_average_te-n_average_te],
#             textposition = 'auto',
#             marker=dict(
#                 color='rgb(158,202,225)',
#                 line=dict(
#                     color='rgb(8,48,107)',
#                     width=1.5),
#             ),
#             opacity=0.6
#         )]
#
# py.iplot(data, filename='Radii Variation for Celulose Concentration')
